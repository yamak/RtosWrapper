#ifndef TASKABLE_H
#define TASKABLE_H

#include "FreeRTOS.h"
#include "task.h"

template <typename task,int _stackSize>
class Taskable:private task {
  
  using task::run;
  
private:
  StackType_t taskStack[_stackSize];
  TaskHandle_t _handle;
  StaticTask_t _tcb;
  
public:
  Taskable():_handle(0){}
  virtual ~Taskable(){}
  void start(const char* name, unsigned int priority)
  {
    _handle=xTaskCreateStatic(run,name,_stackSize,NULL,priority,taskStack,&_tcb);
  }
  
  void suspend(){vTaskSuspend(_handle);}
  
  void resume(){vTaskResume(_handle);}
  
  
};
#endif
