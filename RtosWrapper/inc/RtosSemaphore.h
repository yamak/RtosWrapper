#ifndef RTOS_SEMAPHORE_H
#define RTOS_SEMAPHORE_H
#include "FreeRTOS.h"
#include "semphr.h"

class RtosSemaphore {
  
public:
  RtosSemaphore(TickType_t semTimeout);
  
  bool take();
  
  void give();
  
  void giveFromISR(bool taskWoken);
private:
  SemaphoreHandle_t _semaphore;
  StaticSemaphore_t _semaphoreBuffer;
  TickType_t _timeout;
};
#endif
