#ifndef RTOS_QUEUE_H
#define RTOS_QUEUE_H

#include "FreeRTOS.h"
#include "queue.h"

class RtosQueue {
  
public:
  RtosQueue(unsigned char* buffer, UBaseType_t size, UBaseType_t itemSize);
  
  bool sendFromISR(void* message, bool taskWoken);
  
  bool send(void* message, TickType_t timeout);
  
  bool receive(void* buffer, TickType_t timeout);
  
  void reset();
  
private:
  UBaseType_t _size;
  UBaseType_t _itemSize;
  QueueHandle_t _xQueue;
  unsigned char* _storageBuffer;
  StaticQueue_t _xStaticQueue;
};
#endif
