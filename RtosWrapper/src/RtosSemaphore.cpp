#include "RtosSemaphore.h"
#include <string.h>
RtosSemaphore::RtosSemaphore(TickType_t semTimeout) {
  
  memset(&_semaphoreBuffer,0,sizeof(_semaphoreBuffer));
  this->_semaphore=xSemaphoreCreateBinaryStatic(&_semaphoreBuffer);
  this->_timeout=semTimeout;
   xSemaphoreGive(this->_semaphore);
}

bool RtosSemaphore::take() {
  if(xSemaphoreTake(this->_semaphore,_timeout)==pdTRUE)
    return true;
  else
    return false;
  
}

void RtosSemaphore::give() {
  xSemaphoreGive(this->_semaphore);
  
}

void RtosSemaphore::giveFromISR(bool taskWoken) {
  BaseType_t woken;
  if(taskWoken==true)
    woken=pdTRUE;
  else
    woken=pdFALSE;
  xSemaphoreGiveFromISR(this->_semaphore,&woken);
  
}
