#include "RtosQueue.h"

RtosQueue::RtosQueue(unsigned char* buffer, UBaseType_t size, UBaseType_t itemSize) {
  this->_size=size;
  this->_itemSize=itemSize;
  this->_storageBuffer=buffer;
  this->_xQueue=xQueueCreateStatic(this->_size,this->_itemSize,this->_storageBuffer,&(this->_xStaticQueue));
}

bool RtosQueue::sendFromISR(void* message, bool taskWoken) {
  
  BaseType_t woken;
  if(taskWoken==true)
    woken=pdTRUE;
  else
    woken=pdFALSE;
  if(xQueueSendFromISR(this->_xQueue,message,&woken)==pdTRUE)
    return true;
  else
    return false;
  
}

bool RtosQueue::send(void* message, TickType_t timeout) {
  
  if(xQueueSend(this->_xQueue,message,timeout)==pdTRUE)
    return true;
  else
    return false;
  
}

bool RtosQueue::receive(void* buffer, TickType_t timeout) {
  
  if(xQueueReceive(this->_xQueue,buffer,timeout)==pdTRUE)
    return true;
  else 
    return false;
  
}

void RtosQueue::reset() {
  xQueueReset(this->_xQueue);
  
}
