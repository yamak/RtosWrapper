#include "stm32f4xx.h"
#include <stdio.h>
#include "taskable.h"
#include "RtosSemaphore.h"
RtosSemaphore sem(0xFFFF);
class LedTask1
{
protected:
  static void run(void* pvParameters)
  {
    while(1)
    {
      sem.take();
      for(int i=0;i<10;i++)
      {
        GPIOD->ODR^=~(1<<14);
        vTaskDelay(200);
      }
      sem.give();
      vTaskDelay(50);
    }
  }
};

class LedTask2
{
protected:
  static void run(void* pvParameters)
  {
    while(1)
    {
      sem.take();
      for(int i=0;i<5;i++)
      {
        GPIOD->ODR^=~(1<<12);
        vTaskDelay(100);
      }
      sem.give();
      vTaskDelay(50);
    }
  }
};

Taskable<LedTask1,128> ledTask1;
Taskable<LedTask2,128> ledTask2;
int main()
{
  RCC->AHB1ENR|=0x1F;
  GPIOD->MODER|=0x11000000;
  GPIOD->OSPEEDR|=0x33000000;
  ledTask1.start((const char*)"LED_TASK1",4);
  ledTask2.start((const char*)"LED_TASK2",5);
  vTaskStartScheduler();
  while(1)
  {
    
  }
}

